package plt;

import static org.junit.Assert.*;

import org.junit.Test;

import src.Translator;

public class TraslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}

	@Test
	public void testTranslationPhraseStartWithVowelEndWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartWithUEndWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}

	@Test
	public void testTranslationPhraseStartWithVowelEndWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartWithVowelEndWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartWithConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartWithConsonants() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}	
	
	@Test
	public void testTranslationPhraseWithMoreWordsWithSpace() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}	
	
	@Test
	public void testTranslationPhraseWithMoreWordsComposite() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}	
	
	@Test
	public void testTranslationPhraseWithPunctuations() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}

	@Test
	public void testTranslationPhraseWithUpperCase() {
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		assertEquals("APPLEYAY", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithTitleCase() {
		String inputPhrase = "Hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Ellohay", translator.translate());
	}
}
