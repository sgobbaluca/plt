package src;

public class Translator {

	public static final String NIL = "nil";
	private String phrase;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		String tmpPhrase = "";
		String [] splitPhrase;
		char charSplit;
		Integer punctuationPosition;
		
		if(phrase.contains("-")) {
			splitPhrase = phrase.split("-");
			charSplit = '-';
		}else {
			splitPhrase = phrase.split("\\s");
			charSplit = ' ';
		}

		for(int i=0; i<splitPhrase.length; i++){
			if(i > 0) tmpPhrase += charSplit;
			
			punctuationPosition = containPunctuation(splitPhrase[i]);
			if(punctuationPosition != null) {
				char punctuation;
				
				punctuation = splitPhrase[i].charAt(punctuationPosition);
				splitPhrase[i] = splitPhrase[i].substring(0, punctuationPosition) + splitPhrase[i].substring(punctuationPosition+1, splitPhrase[i].length());
				tmpPhrase += translateWord(splitPhrase[i]);
				tmpPhrase += punctuation;
				
			}else {
				tmpPhrase += translateWord(splitPhrase[i]);
			}
			
		}
		
		return tmpPhrase;
	}
	
	private String translateWord(String word) {
		int letterCase = 0; // 0 = lower-case, 1 = title-case, 2 = upper-case
		
		if(word.length() == 0) return NIL;
		
		if(Character.isUpperCase(word.charAt(0))) {
			letterCase = 1;
			if(isAllUpperCase(word)) {
				letterCase = 2;
			}
		}
		
		word = word.toLowerCase();
		
		if(startWithVowel(word)) {
			if(word.endsWith("y")) {
				word += "nay";
			}else if(endWithVowel(word)) {
				word += "yay";
			}else if(!endWithVowel(word)) {
				word += "ay";
			}
		}else if(word.length() != 0){
			while(!startWithVowel(word)){
				word = word.substring(1, word.length()) + word.substring(0, 1);
			}
			
			word += "ay";
		}
		
		if(letterCase == 1) word = word.substring(0, 1).toUpperCase() + word.substring(1);
		else if(letterCase == 2) word = word.toUpperCase();
		
		return word;
	}
	
	private Integer containPunctuation(String word) {
		if(word.contains(".")) return word.indexOf(".");
		else if(word.contains(",")) return word.indexOf(",");
		else if(word.contains(";")) return word.indexOf(";");
		else if(word.contains(":")) return word.indexOf(":");
		else if(word.contains("?")) return word.indexOf("?");
		else if(word.contains("!")) return word.indexOf("!");
		else if(word.contains("'")) return word.indexOf("'");
		else if(word.contains("(")) return word.indexOf(")");
		else if(word.contains(")")) return word.indexOf(")");
		
		return null;
		
	}
	
	private boolean isAllUpperCase(String word) {
		for(int i = 0; i<word.length(); i++) {
			if(Character.isLowerCase(word.charAt(i))) return false;
		}
		return true;
	}
	
	private boolean startWithVowel(String word) {
		return word.startsWith("a") || word.startsWith("e") || word.startsWith("i") || word.startsWith("o") || word.startsWith("u");
	}

	private boolean endWithVowel(String word) {
		return word.endsWith("a") || word.endsWith("e") || word.endsWith("i") || word.endsWith("o") || word.endsWith("u");
	}
	
}
